package cn.deali.minimalpoemkotlin.entity

class Poem {
    var dynasty: String = ""
    var title: String = ""
    var author: String = ""
    var content: String = ""
}